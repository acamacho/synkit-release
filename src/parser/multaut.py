#!/usr/bin/env python3

from translator.automata import get_automaton,get_automata
from os import system
import time

def negate(spec):
    return "!(%s)" % spec.strip()

def to_conjunction_form(specs_dict,opts):
    clauses = []
    
    if 'preset' in specs_dict:
        if 'initially' in specs_dict:
            initially = ' && '.join("({})".format(spec) for spec in specs_dict['initially'])
            
            for preset in specs_dict['preset']:
                clause = "(({}) || ({}))".format(negate(initially) , preset)
                clauses.append(clause)
        else:
            for preset in specs_dict['preset']:
                clauses.append(preset)
    if 'system' in specs_dict:
        assumptions = None
        if 'initially' in specs_dict:
            if 'environment' in specs_dict:
                assumptions = ' && '.join("({})".format(spec) for spec in specs_dict['environment'] + specs_dict['initially'])
        else:
            if 'environment' in specs_dict:
                assumptions = ' && '.join("({})".format(spec) for spec in specs_dict['environment'] )
        if assumptions:
            for guarantee in specs_dict['system']:
                clause = "(({}) || ({}))".format(negate(assumptions) , guarantee)
                clauses.append(clause)
        else:
            for guarantee in specs_dict['system']:
                clauses.append(guarantee)
    
    return clauses
    
def to_disjunction_form(specs_dict,opts):
    clauses = []
    
    if 'initially' in specs_dict:
        for clause in specs_dict['initially']:
            clauses.append(negate(clause))
    if 'environment' in specs_dict:
        if 'presets' in specs_dict:
            presets = ' && '.join("({})".format(spec) for spec in specs_dict['preset'])
            for clause in specs_dict['environment']:
                clause = "(({}) && ({}))".format(presets, negate(clause))
                clauses.append(clause)
    guarantees = []
    if 'system' in specs_dict:
        guarantees.extend(specs_dict['system'])
    if 'presets' in specs_dict:
        guarantees.extend(specs_dict['presets'])
    if len(guarantees) > 0:
        clause = ' && '.join("({})".format(spec) for spec in guarantees)
        clauses.append(clause)
        
    return clauses
    
class MultAut:
    def __init__(self, specs_dict, opts):
        self.automata = []
        aut_id = 0
        self.automata_time = None
        start = time.time()

        # auxiliar for statistics
        self.subformulae_dict = {}
        
        legal_keys = ['initially', 'preset', 'environment', 'system']
        
        if opts.aut_type == "ucw":
            specs_list = to_conjunction_form(specs_dict,opts)
        elif opts.aut_type == "nbw":
            specs_list = to_disjunction_form(specs_dict,opts)
        else:
            assert(opts.aut_type == "nfw")
            if not opts.check_unrealizability:
                specs_list = to_conjunction_form(specs_dict,opts)
            elif opts.check_unrealizability:
                # turn the negation of spec into a disjunction
                specs_list = to_conjunction_form(specs_dict,opts)
                specs_list = [negate(spec) for spec in specs_list]
            # self.automata_dict = {}
            # self.automata_dict["agent"] = range(len(specs_list))
            
            # TODO: implement it as an optional feature
            # 
            # # negate spec
            # if not opts.check_unrealizability:
            #     specs_list = to_disjunction_form(specs_dict,opts)
            # elif opts.check_unrealizability:
            #     # turn the negation of spec into a disjunction
            #     specs_list = to_conjunction_form(specs_dict,opts)
            #     # negate each subspec term, so now it is a disjunction
            #     specs_list = [negate(spec) for spec in specs_list]
            
        for spec in specs_list:
            varformula,varlist,automata = '_G100', '[_G100]', [get_automaton(spec, opts.aut_type, opts.ltl2aut, opts.from_ltlf)]
            varlist = varlist.replace('[','').replace(']','').strip().split(',')
            temp_dict = {}
            assert(len(automata) == len(varlist))
            for i in range(len(automata)):
            # for automaton in automata:
                self.automata.append(automata[i])
                print("\tGenerated %s automaton %d of size %d" % (opts.aut_type,aut_id, automata[i].num_states()))
                aut_id += 1 
        
        self.automata_time = time.time() - start
        print("Automata generation time: %s" % self.automata_time)
        