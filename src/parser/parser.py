import os
import subprocess

SYFCO_EXE = os.path.join(os.path.dirname(__file__), os.path.join('syfco','syfco'))
# OUTPUT_FORMAT = "ltlxba"
# OUTPUT_FORMAT = "acacia"
# OUTPUT_FORMAT = "acacia-specs"


def acacia2ltlxba(spec):
    assumptions = []
    guarantees = []
    for constraint in spec.split(';'):
        if "assume" in constraint:
            assumptions.append( constraint.replace("assume ","") )
        elif len(constraint.strip() ) > 0:
            guarantees.append( constraint )
    if len(assumptions) > 0:
        ltl_spec = "( %s ) -> ( %s ) " % ( " && ".join(assumptions),  " && ".join(guarantees)  )
    else:
        ltl_spec = " && ".join(guarantees)
    # print(ltl_spec)
    return ltl_spec.replace("\n","")
    
def parse_synplan_specs(spec):
    spec_dict = {}
    keys = ['neg_initially', 'preset', 'neg_environment', 'agent']
    for constraint in spec.split(';'):
        for key in keys:
            if key in constraint:
                if key not in spec_dict:
                    spec_dict[key] = []
                fml = constraint.replace(key,"").replace(' ','').strip() 
                spec_dict[key].append( fml )
                break
    return spec_dict
    
def parse_synkit_specs(spec):
    spec_dict = {}
    keys = ['initially', 'preset', 'environment', 'system']
    for constraint in spec.split(';'):
        for key in keys:
            if key in constraint:
                if key not in spec_dict:
                    spec_dict[key] = []
                fml = constraint.replace(key,"").replace(' ','').strip() 
                spec_dict[key].append( fml )
                break
    return spec_dict

class Parser():
    def __init__(self,tlsf_file):
        self.tlsf_file = tlsf_file
        
    def uncontrollable_vars(self):
        # returns the input variables in the TLSF file
        command = "%s -f ltlxba -ins %s" % (SYFCO_EXE, self.tlsf_file)
        syfco_proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
        (syfco_out, syfco_err) = syfco_proc.communicate()
        assert(not syfco_err)
        return [x.replace(" ","") for x in str(syfco_out,'utf-8').strip().split(",")]

    def controllable_vars(self):
        # returns the output variables in the TLSF file
        command = "%s -f ltlxba -outs %s" % (SYFCO_EXE, self.tlsf_file)
        syfco_proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
        (syfco_out, syfco_err) = syfco_proc.communicate()
        assert(not syfco_err)
        return [x.replace(" ","") for x in str(syfco_out,'utf-8').strip().split(",")]
    
    def specs_object(self, opts):
        automata_split = opts.split_spec
        # returns a vector with the specficication formulae to satisfy
        # code borrowed from SYFCO
        if opts.from_ltlf:
            if opts.split_spec in ["none","gradual"]:
                output_format = 'synplan'
                
                # command = "%s -f %s -m fully -nnf %s" % (SYFCO_EXE, output_format, self.tlsf_file)
                command = "%s -f %s -m fully %s" % (SYFCO_EXE, output_format, self.tlsf_file)
                # print(command)
                syfco_proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
                (syfco_out, syfco_err) = syfco_proc.communicate()
                assert(not syfco_err)
    
                spec = str(syfco_out, 'utf-8').replace(' ','')
                return [spec]
            elif automata_split == "icaps18tests_components":
                # TODO: remove this after submission to ICAPS18
                output_format = "full"
                command = "%s -f %s -m fully %s" % (SYFCO_EXE, output_format, self.tlsf_file)
                # print(command)
                syfco_proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
                (syfco_out, syfco_err) = syfco_proc.communicate()
                spec = str(syfco_out, 'utf-8')
                
                specs = spec.split("GUARANTEES {")[1].split("}")[0].strip().split(';')
                specs = [spec.strip() for spec in specs if len(spec.strip()) > 0]
                
                # print(specs)
                # print(len(specs));exit()
                spec_dict = {}
                spec_dict["system"] = specs
                # print(specs)
                # exit
                return spec_dict
                
            elif automata_split == "tlsf_components":
                # TODO revert to this after submission to ICAPS18
                output_format = 'synkit'
                command = "%s -f %s -m fully %s" % (SYFCO_EXE, output_format, self.tlsf_file)
                syfco_proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
                (syfco_out, syfco_err) = syfco_proc.communicate()
                assert(not syfco_err)
                spec = str(syfco_out, 'utf-8')
                spec_dict = parse_synkit_specs(spec)
                # import json
                # print(json.dumps(spec_dict, indent=4))
                return spec_dict
            else:
                assert(opts.split_spec in ["nnf", "tlsf"])
                output_format = 'synplan-specs'
                command = "%s -f %s -m fully %s" % (SYFCO_EXE, output_format, self.tlsf_file)
                syfco_proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
                (syfco_out, syfco_err) = syfco_proc.communicate()
                assert(not syfco_err)
                spec = str(syfco_out, 'utf-8')
                spec_dict = parse_synplan_specs(spec)
                # import json
                # print(json.dumps(spec_dict, indent=4))
                return spec_dict
        
        elif automata_split == "tlsf_components":
            output_format = 'synkit'
        elif automata_split == "acacia-specs":
            output_format = 'acacia-specs'
        else:
            output_format = 'ltlxba'
        command = "%s -f %s -m fully %s" % (SYFCO_EXE, output_format, self.tlsf_file)
        syfco_proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
        (syfco_out, syfco_err) = syfco_proc.communicate()
        assert(not syfco_err)
        if output_format == 'synkit':
            command = "%s -f %s -m fully %s" % (SYFCO_EXE, output_format, self.tlsf_file)
            syfco_proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
            (syfco_out, syfco_err) = syfco_proc.communicate()
            assert(not syfco_err)
            spec = str(syfco_out, 'utf-8')
            spec_dict = parse_synkit_specs(spec)
            # import json
            # print(json.dumps(spec_dict, indent=4))
            return spec_dict
        elif output_format == 'ltlxba':
            spec = str(syfco_out, 'utf-8')
            return [spec]
        elif output_format == 'acacia':
            spec = str(syfco_out, 'utf-8')
            return [acacia2ltlxba(spec)]
        elif output_format == 'acacia-specs':
            spec = str(syfco_out, 'utf-8')
            specs = []
            spec_unit_idx = 0
            init_idx = spec.find( "[spec_unit %s]" % spec_unit_idx ) + len("[spec_unit %s]" % spec_unit_idx)
            spec_unit_idx += 1
            end_idx = spec.find( "[spec_unit %s]" % spec_unit_idx )
            while( end_idx != -1):
                spec_unit = acacia2ltlxba(spec[init_idx:end_idx])
                specs.append(spec_unit)
                init_idx = spec.find( "[spec_unit %s]" % spec_unit_idx ) + len("[spec_unit %s]" % spec_unit_idx)
                spec_unit_idx += 1
                end_idx = spec.find( "[spec_unit %s]" % spec_unit_idx )
            spec_unit = acacia2ltlxba(spec[init_idx:])
            specs.append(spec_unit)
            return specs
        else:
            raise ("Fomat %s not yet supported." % output_format)
            
    def get_alanet(self,opts):
        from parser.alanet import ALANet
        assert(opts.aut_type in ["nfw","ucw","nbw"])
        
        specs_object = self.specs_object(opts)
        if opts.split_spec in ["none","gradual"]:
            specs_dict = {}
            specs_dict["agent"] = specs_object
            return ALANet(specs_dict,opts)
        elif opts.split_spec == "tlsf_components":
            from parser.multaut import MultAut
            return MultAut(specs_object,opts)
        else:
            assert(opts.split_spec in ["nnf","tlsf"])
            return ALANet(specs_object,opts)