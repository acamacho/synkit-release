# This file is part of SynKit, a tool for LTL synthesis via automated planning.
# Copyright (C) 2018  Alberto Camacho <acamacho@cs.toronto.edu>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

def compute_mutexes(variables, automata):
    # returns a matrix M where M[l] is the set of automata transitions
    # that are mutex with a literal l in Literals(variables)
    mutexes = {}
    for v in variables:
        mutexes[v] = []
        mutexes['!' + v] = []
        for aut_dynamics in automata:
            mutexes[v] += get_mutex_transitions_for_literal(aut_dynamics, v)
            mutexes['!' + v] += get_mutex_transitions_for_literal(aut_dynamics, '!' + v)
    return mutexes

def get_mutex_transitions_for_literal(aut_dynamics, literal):
    # returns the set of transitions in the given automaton 
    # that are mutex with the given literal
    if '!' in literal:
        neg_literal = literal[1:]
    else:
        neg_literal = '!' + literal
    mutexes = []
    transitions = aut_dynamics.aut.get_transitions()
    for t in transitions:
        if neg_literal in t.guard:
            # print("%s in %s.guard=%s" % (neg_literal,t._id, t.guard))
            # mutexes.append(t)
            mutexes.append(aut_dynamics.t(t._id))
    return set(mutexes)

def get_fixed_var_ordering_preconds_for_controllable_vars_action(deleted_transition_fluents):
    aux_dict = {}
    for transition_fluent in deleted_transition_fluents:
        # "aut_%s_t_%s" % (self.aut_id, t_id)
        aut_id = int(transition_fluent.split('_')[1])
        t_id = int(transition_fluent.split('_')[3])
        if aut_id not in aux_dict:
            aux_dict[aut_id] = t_id
        elif aux_dict[aut_id] > t_id:
            aux_dict[aut_id] = t_id
    fixed_var_ordering_preconds = []
    for aut_id in aux_dict:
        fixed_var_ordering_preconds += ["(not (poss aut_%s_t_%s))" % (aut_id, j) for j in range(aux_dict[aut_id])]
    
    return fixed_var_ordering_preconds

def num(m):
    return "alphacount_%s" % m

def arity(w):
    return "arity_%s" % w
    
def aut(a):
    return "aut_%s" % a
    
class Options:
    def __init__(self,opts):
        self.opts = opts
        assert(opts.k_coBuchi!=None)
        assert(opts.output_domain)
        assert(opts.output_instance)
        assert(opts.fixed_var_ordering!=None)
        
    def k_coBuchi(self):
        return self.opts.k_coBuchi
    
    def output_domain(self):
        # return "%s_domain.pddl" % self.opts.output
        return self.opts.output_domain

    def output_instance(self):
        # return "%s_instance.pddl" % self.opts.output
        return self.opts.output_instance
    
    def fixed_var_ordering(self):
        return self.opts.fixed_var_ordering
        
    def decompose_automata(self):
        return self.opts.split_spec
    
    def game_semantics(self):
        return self.opts.game_semantics

    def output_controller(self):
        return True

class Action:
    name = None
    param = None
    pre = None
    effs = None
    def to_pddl(self):
        assert(len(self.effs) > 0)
        pddl = "\n\t(:action %s" % self.name
        if self.param:
            pddl += "\n\t\t:parameters %s" % self.param
        precs_pddl = "(and %s)" % (" ".join(self.pre))
        if len(self.effs) > 1:
            effs_pddl = "(oneof \n\t\t\t" + "\n\t\t\t".join( "(and %s)" %  " ".join(eff_k) for eff_k in self.effs  ) + "\n\t\t\t)\n\t\t"
        elif len(self.effs) == 1:
            effs_pddl = "(and %s)" % " ".join(self.effs[0])
        pddl += "\n\t\t:precondition %s" % precs_pddl
        pddl += "\n\t\t:effect %s" % effs_pddl
        pddl +="\n\t)"

        return pddl


class AutomatonDynamics():
    def __init__(self, aut, aut_id):
        self.aut = aut
        self.aut_id = aut_id
        
    def q(self,k):
        return "aut_%s_q_%s" % (self.aut_id, k)

    def t(self,t_id):
        return "aut_%s_t_%s" % (self.aut_id, t_id)
    
class PDDLDynamics():
    def __init__(self, uncontrollable_vars, controllable_vars, opts):
        self.controllable_vars = controllable_vars
        self.uncontrollable_vars = uncontrollable_vars
        self.automata = []
        self.opts = Options(opts)
    
    def add_automaton(self,aut):
        self.automata.append(AutomatonDynamics(aut, len(self.automata)))
    
    def write_pddl_domain(self):
        print("Writing PDDL Domain")
        domain_file = open(self.opts.output_domain(),"w")
        mutexes = compute_mutexes(self.controllable_vars + self.uncontrollable_vars, self.automata)
        
        self.write_header(domain_file)
        
        self.write_environment_actions(domain_file, mutexes)
        self.write_controllable_vars_action(domain_file,mutexes)
            
        self.write_automata_actions(domain_file, mutexes)
        self.write_start_sync_action(domain_file)
        self.write_sync_F_actions(domain_file)
        self.write_continue_action(domain_file)
        domain_file.write(")")
        domain_file.close()
    
    def write_pddl_instance(self):
        print("Writing PDDL Instance")
        instance_file = open(self.opts.output_instance(),"w")

        init = "\n\t" + " ".join("(nxt %s %s)" % (num(n+1), num(n))  for n in range(self.opts.k_coBuchi()))
        init += "\n\t" + "(turn sync)"
        for aut_dynamics in self.automata:
            Q_Fin = aut_dynamics.aut.get_accepting()
            q_init = aut_dynamics.aut.get_init_state_number()

            for transition in aut_dynamics.aut.get_transitions():
                if transition.src_id == q_init:
                    init += " (poss %s)" % aut_dynamics.t(transition._id)
                if transition.dst_id not in Q_Fin:
                    init += " (safe %s)" % aut_dynamics.t(transition._id)
                
            init += " (oldCnt %s %s)" % (aut_dynamics.q(q_init), num(int(q_init in Q_Fin)))
        
        header = """(define (problem prob)\n\t(:domain dom)\n\t(:init %s)\n\t(:goal (dummy_goal))\n)""" % (init)
        instance_file.write(header)
        instance_file.close()
        
    def write_header(self, pddl_file):
        # transition type
        self.types = "\n\t" + " autstate"
        self.types += "\n\t" + " transition"
        self.types += "\n\t" + " alphacounter"
        self.types += "\n\t" + " var"

        self.constants = "\n"
        for aut_dynamics in self.automata:
            for k in range(0, aut_dynamics.aut.num_states()):
                self.constants += "\n\t%s - autstate" % aut_dynamics.q(k)

        for aut_dynamics in self.automata:
            for transition in aut_dynamics.aut.get_transitions():
                self.constants += "\n\t%s - transition" % aut_dynamics.t(transition._id)

        for var in self.uncontrollable_vars + self.controllable_vars:
            self.constants += "\n\t%s - var" % var
        self.constants += "\n\ty - var"
        self.constants += "\n\tsync - var"
        
        for n in range(self.opts.k_coBuchi()+1):
            self.constants += "\n\t%s - alphacounter" % num(n)
        
        self.predicates = "\n\t" + "(safe ?t - transition)"
        self.predicates += "\n\t" + "(poss ?t - transition)"
        self.predicates += "\n\t" + "(turn ?n - var)"

        self.predicates += "\n\t" + "(F ?q - autstate)"
        self.predicates += "\n\t" + "(oldCnt ?q - autstate ?m - alphacounter)"
        self.predicates += "\n\t" + "(newCnt ?q - autstate ?m - alphacounter)"
        self.predicates += "\n\t" + "(nxt ?n - alphacounter ?m - alphacounter)"

        self.predicates += "\n\t" + "(dummy_goal)"

        header = """(define (domain dom)
        (:requirements :typing :strips :negative-preconditions :non-deterministic :adl)
        (:types %s)
        (:constants %s)
        (:predicates %s)
        """ % (self.types, self.constants, self.predicates)
        pddl_file.write(header)

    def write_automata_actions(self, pddl_file, mutexes):
        for aut_dynamics in self.automata:
            self.write_automaton_actions(aut_dynamics,pddl_file, mutexes)
            
    def write_automaton_actions(self, aut_dynamics, pddl_file, mutexes):
        # Write actions to set Y variables via automaton transitions
        Q_Fin = aut_dynamics.aut.get_accepting()
        transitions = aut_dynamics.aut.get_transitions()
        # print(Q_Fin)
        for t in transitions:
            # print("%s-->%s \t %s" % (t.src_id, t.dst_id,t.guard))
            action = self.write_transition_action(aut_dynamics, t, t.dst_id in Q_Fin, mutexes)
            pddl_file.write(action.to_pddl())
    
    def write_transition_action(self, aut_dynamics, transition, goesto_accepting, mutexes):
        action = Action()
        if goesto_accepting:
            action.name = "move_unsafe_aut_%s_t%s_%s_%s" % (aut_dynamics.aut_id, transition._id, transition.src_id, transition.dst_id)
            action.param = "(?m - alphacounter ?n - alphacounter)"
            action.pre = ['(turn y)', '(poss %s)' % aut_dynamics.t(transition._id), '(not (safe %s))' % aut_dynamics.t(transition._id), '(oldCnt %s ?m)' % aut_dynamics.q(transition.src_id), '(nxt ?n ?m)']
            eff = ['(F %s)' % aut_dynamics.q(transition.dst_id), '(newCnt %s ?n)' % aut_dynamics.q(transition.dst_id), '(not (poss %s))' % aut_dynamics.t(transition._id)]
        else:
            action.name = "move_safe_aut_%s_t%s_%s_%s" % (aut_dynamics.aut_id, transition._id, transition.src_id, transition.dst_id)
            action.param = "(?m - alphacounter)"
            action.pre = ['(turn y)', '(poss %s)' % aut_dynamics.t(transition._id), '(safe %s)' % aut_dynamics.t(transition._id), '(oldCnt %s ?m)' % aut_dynamics.q(transition.src_id)]
            eff = ['(F %s)' % aut_dynamics.q(transition.dst_id), '(newCnt %s ?m)' % aut_dynamics.q(transition.dst_id), '(not (poss %s))' % aut_dynamics.t(transition._id)]

        if self.opts.fixed_var_ordering():
            action.pre += ['(not (poss %s))' % aut_dynamics.t(j) for j in range(transition._id)]

        for t in aut_dynamics.aut.get_transitions():
            if t.src_id == transition.src_id and t.dst_id == transition.dst_id and t._id != transition._id:
                eff += ['(not (poss %s))' % aut_dynamics.t(t._id)]

        # transition_mutexes = []
        # for literal in transition.guard:
        #     transition_mutexes += mutexes[literal]
        # eff += ['(not (poss %s))' % t for t in set(transition_mutexes)]
        # eff += ['(set %s)' % aut_dynamics.v(literal) for literal in transition.guard]

        action.effs = [eff]
        return action

    def write_controllable_vars_action(self, pddl_file, mutexes):
        # Write cascade of actions to set Y variables
        for i in range(len(self.controllable_vars)):
            if i < len(self.controllable_vars) - 1:
                next_turn = "(turn %s)" % self.controllable_vars[i+1]
            else:
                if self.opts.game_semantics() == "mealy":
                    next_turn = "(turn y)"
                else:
                    assert(self.opts.game_semantics() == "moore")
                    next_turn = "(turn %s)" % self.uncontrollable_vars[0]
            
            # deterministic actions
            action = Action()
            action.name = "enable_var_%s" % self.controllable_vars[i]
            action.pre = [ "(turn %s)" % self.controllable_vars[i] ]
            eff = ["(not (turn %s))" % self.controllable_vars[i], next_turn]
            eff += ["(not (poss %s))" % transition for transition in mutexes[self.controllable_vars[i]]]
            action.effs = [eff]
            pddl_file.write(action.to_pddl())

            action = Action()
            action.name = "disable_var_%s" % self.controllable_vars[i]
            action.pre = ["(turn %s)" % self.controllable_vars[i] ]
            eff = ["(not (turn %s))" % self.controllable_vars[i], next_turn]
            eff += ["(not (poss %s))" % transition for transition in mutexes['!' + self.controllable_vars[i]]]
            action.effs = [eff]
            pddl_file.write(action.to_pddl())
        
    def write_start_sync_action(self, pddl_file):
        # Write action to switch mode from environment_mode to automaton_mode
        action = Action()
        action.name = "start_sync"
        action.pre = ["(turn y)"]
        action.pre += ["(forall (?t - transition) (not (poss ?t)))"]
        eff = ["(turn sync)", "(not (turn y))"] 
        eff += ["(forall (?q - autstate  ?m  - alphacounter) (not (oldCnt ?q ?m ) ) )"]
        action.effs = [eff]
        pddl_file.write(action.to_pddl())    

    def write_sync_F_actions(self, pddl_file):
        for aut_dynamics in self.automata:
            for n_q in range(aut_dynamics.aut.num_states()):
                for m in range(self.opts.k_coBuchi()+1):
                    action = Action()
                    action.name = "sync_F_aut_%s_q_%s_alphacount_%s" % (aut_dynamics.aut_id, n_q, m)
                    action.pre = ["(turn sync)", "(F %s)" %  aut_dynamics.q(n_q), "(newCnt %s %s)" % (aut_dynamics.q(n_q), num(m))]
                    action.pre += ["(not (newCnt %s %s))" % (aut_dynamics.q(n_q), num(n)) for n in range(m+1,self.opts.k_coBuchi()+1)]
                    if self.opts.fixed_var_ordering():
                        action.pre += ['(not (F %s))' % aut_dynamics.q(j) for j in range(n_q)]
                    eff = [] 
                    for transition in aut_dynamics.aut.get_transitions():
                        if transition.src_id == n_q:
                            eff += ["(poss %s)" % aut_dynamics.t(transition._id)]
                    eff += ["(oldCnt %s %s)" % (aut_dynamics.q(n_q), num(m)), "(not (F %s))" %  aut_dynamics.q(n_q)]
                    eff += ["(forall (?n - alphacounter) (not (newCnt %s ?n)))" % aut_dynamics.q(n_q)]
                    
                    action.effs = [eff]
                    pddl_file.write(action.to_pddl())    
        
    def write_continue_action(self, pddl_file):
        if self.opts.game_semantics() == "mealy":
            next_turn = "(turn %s)" % self.uncontrollable_vars[0]
        else:
            assert(self.opts.game_semantics() == "moore")
            next_turn = "(turn %s)" % self.controllable_vars[0]
        
        action = Action()
        action.name = "continue"
        action.pre = ["(turn sync)"]
        action.pre += ["(forall (?q - autstate) (not (F ?q)))"]
        eff1 = ["(dummy_goal)"] 
        eff2 = [next_turn, "(not (turn sync))"]
        
        action.effs = [eff1,eff2]
        pddl_file.write(action.to_pddl())
        
    def write_environment_actions(self, pddl_file, mutexes):
        # Write cascade of actions to set X variables
        for i in range(len(self.uncontrollable_vars)):
            if i < len(self.uncontrollable_vars) - 1:
                next_turn = "(turn %s)" % self.uncontrollable_vars[i+1]
            else:
                if self.opts.game_semantics() == "mealy":
                    next_turn = "(turn %s)" % self.controllable_vars[0]
                else:
                    assert(self.opts.game_semantics() == "moore")
                    next_turn = "(turn y)"
            
            # non-deterministic actions
            action = Action()
            action.name = "env_move_%s" % self.uncontrollable_vars[i]
            action.pre = ["(turn %s)" % self.uncontrollable_vars[i] ]
            
            eff1 = ["(not (turn %s))" % self.uncontrollable_vars[i], next_turn ]
            eff2 = ["(not (turn %s))" % self.uncontrollable_vars[i], next_turn ]

            eff1 += ["(not (poss %s))" % transition for transition in mutexes[self.uncontrollable_vars[i]]]
            eff2 += ["(not (poss %s))" % transition for transition in mutexes['!' + self.uncontrollable_vars[i]]]
            action.effs = [eff1,eff2]
            pddl_file.write(action.to_pddl())
        
    # for statistical analysis
    def statistics(self):
        return 0,0
        # number of ground fluents
        #
        n_fluents = 0
        n_fluents += sum(aut_dynamics.aut.num_states() for aut_dynamics in self.automata)
        n_fluents += sum( len(aut_dynamics.aut.get_transitions()) for aut_dynamics in self.automata)
        n_fluents += len(self.uncontrollable_vars) #+ len(self.controllable_vars)
        n_fluents += len(['y','sync'])
        if self.opts.decompose_automata():
            n_fluents += len(self.get_all_triggers())
        else:
            n_fluents += len(['dummy_goal'])
        
        
        # number of ground actions
        
        n_actions = 0
        n_actions += len(self.uncontrollable_vars)
        n_actions += sum( len(aut_dynamics.aut.get_transitions()) for aut_dynamics in self.automata )
        if self.opts.decompose_automata():
            legal_keys = ['neg_initially', 'preset', 'neg_environment', 'agent']
            for key in self.alanet.subtree_dict:
                if key in ['neg_initially', 'neg_environment']:
                    n_actions += len( self.alanet.subtree_dict[key])
                else:
                    n_actions += 1
            for key in self.alanet.subtree_dict:
                subtrees = self.alanet.subtree_dict[key]
                for subtree in subtrees:
                    n_actions += self.num_actions_in_subtree(subtree)
            if "preset" in self.alanet.subtree_dict and ("neg_environment" in self.alanet.subtree_dict or "agent" in self.alanet.subtree_dict):
                n_actions += 1
        n_actions += 1
            
        for aut_dynamics in self.automata:
            for n_q in range(aut_dynamics.aut.num_states()):
                n_actions += 1
                
        n_actions += 1
    
        return n_fluents,n_actions
            
    def num_actions_in_subtree(self,node):
        n = 0
        if node.connective == "and":
            n = 1
        elif node.connective == "or":
            n = len(node.siblings)
            
        if node.siblings != None:
            for sibling in node.siblings:
                n += self.num_actions_in_subtree(sibling)
        
        return n
                
class SASpDynamics():
    def __init__(self, uncontrollable_vars, controllable_vars, opts):
        self.controllable_vars = controllable_vars
        self.uncontrollable_vars = uncontrollable_vars
        self.automata = []
        self.opts = Options(opts)
    
    def add_automaton(self,aut):
        self.automata.append(AutomatonDynamics(aut, len(self.automata)))
        
    def write_version(self,sasp_file):
        version = "begin_version\n3.FOND\nend_version"
        sasp_file.write(version)

    def write_metric(self,sasp_file):
        metric = "\nbegin_metric\n0\nend_metric"
        sasp_file.write(metric)
    
    def write_variables(self,sasp_file,varNames, varName2varNum, varName2Value):
        
        # todo: clear dictionaries and varNames list

        # start
        n_vars = 0
        sasp_vars = []
        kLimit = self.opts.k_coBuchi()
        # turn variables (this one is multivalued)
        turn_vars = ["(turn %s)" % var for var in self.uncontrollable_vars + self.controllable_vars] + ["(turn y)", "(turn sync)"]
        
        for i in range(len(turn_vars)):
            varName2varNum[turn_vars[i]] = n_vars
            varName2Value[turn_vars[i]] = i
            
        # write turn variables to sasp_file
        var_name = "(turn)"
        legal_var_name = "var%s_%s" % (n_vars,var_name)
        range_names = '\n'.join("Atom %s" % var for var in turn_vars)
        sasp_var = "\nbegin_variable\n%s\n-1\n%s\n%s\nend_variable" % (legal_var_name,len(turn_vars), range_names)
        # sasp_file.write(sasp_var)
        sasp_vars.append(sasp_var)
        
        # update n_vars
        n_vars += 1
        varNames.append(var_name)
        
        ##############
        #############
        
        # automaton variables
        for aut_dynamics in self.automata:
            for k in range(0, aut_dynamics.aut.num_states()):
                var_name = "(F %s)" % aut_dynamics.q(k)
                legal_var_name = "var%s_%s" % (n_vars,var_name)
                varName2varNum[var_name] = n_vars
                varName2Value[var_name] = 1
                
                # write automaton variable to sasp_file
                range_names = '\n'.join(["NegatedAtom %s" % var_name, "Atom %s" % var_name])
                sasp_var = "\nbegin_variable\n%s\n-1\n%s\n%s\nend_variable" % (legal_var_name,2, range_names)
                # sasp_file.write(sasp_var)
                sasp_vars.append(sasp_var)
                
                # update n_vars
                n_vars += 1
                varNames.append(var_name)
                

        ##############
        #############

        # transition variables
        for aut_dynamics in self.automata:
            for transition in aut_dynamics.aut.get_transitions():
                var_name = "(poss %s)" % aut_dynamics.t(transition._id)
                legal_var_name = "var%s_%s" % (n_vars,var_name)
                varName2varNum[var_name] = n_vars
                varName2Value[var_name] = 1
                
                # write automaton variable to sasp_file
                range_names = '\n'.join(["NegatedAtom %s" % var_name, "Atom %s" % var_name])
                sasp_var = "\nbegin_variable\n%s\n-1\n%s\n%s\nend_variable" % (legal_var_name,2, range_names)
                # sasp_file.write(sasp_var)
                sasp_vars.append(sasp_var)
                
                # update n_vars
                n_vars += 1
                varNames.append(var_name)

        
        # counters
        for varname in ["oldCnt", "newCnt"]:
            for aut_dynamics in self.automata:
                for s in range(0, aut_dynamics.aut.num_states()):
                    
                    for k in range(kLimit+1):
                        var_name = "(%s %s %s)" % (varname,aut_dynamics.q(s),num(k))
                        
                        legal_var_name = "var%s_%s" % (n_vars,var_name)
                        varName2varNum[var_name] = n_vars
                        varName2Value[var_name] = 1
                        
                        # write automaton variable to sasp_file
                        range_names = '\n'.join(["NegatedAtom %s" % var_name, "Atom %s" % var_name])
                        sasp_var = "\nbegin_variable\n%s\n-1\n%s\n%s\nend_variable" % (legal_var_name,2, range_names)
                        # sasp_file.write(sasp_var)
                        sasp_vars.append(sasp_var)
                        
                        # update n_vars
                        n_vars += 1
                        varNames.append(var_name)      
                        
        for k in range(kLimit):
            var_name = "(nxt %s %s)" % (k+1,k)
            legal_var_name = "var%s_%s" % (n_vars,var_name)
            varName2varNum[var_name] = n_vars
            varName2Value[var_name] = 1
            
            # write automaton variable to sasp_file
            range_names = '\n'.join(["NegatedAtom %s" % var_name, "Atom %s" % var_name])
            sasp_var = "\nbegin_variable\n%s\n-1\n%s\n%s\nend_variable" % (legal_var_name,2, range_names)
            # sasp_file.write(sasp_var)
            sasp_vars.append(sasp_var)
            
            # update n_vars
            n_vars += 1
            varNames.append(var_name)
            
        
        var_name = "(dummy_goal)"
        legal_var_name = "var%s_%s" % (n_vars,var_name)
        varName2varNum[var_name] = n_vars
        varName2Value[var_name] = 1
        
        # write automaton variable to sasp_file
        range_names = '\n'.join(["NegatedAtom %s" % var_name, "Atom %s" % var_name])
        sasp_var = "\nbegin_variable\n%s\n-1\n%s\n%s\nend_variable" % (legal_var_name,2, range_names)
        # sasp_file.write(sasp_var)
        sasp_vars.append(sasp_var)
        
        # update n_vars
        n_vars += 1
        varNames.append(var_name)
 
        ##############
        #############

        if self.opts.output_controller():
            var_name = "(pos)"
            legal_var_name = "var%s_%s" % (n_vars,var_name)
            varName2varNum[var_name] = n_vars
            varName2Value[var_name] = 1
            
            # write automaton variable to sasp_file
            range_names = '\n'.join(["NegatedAtom %s" % var_name, "Atom %s" % var_name])
            sasp_var = "\nbegin_variable\n%s\n-1\n%s\n%s\nend_variable" % (legal_var_name,2, range_names)
            # sasp_file.write(sasp_var)
            sasp_vars.append(sasp_var)
            
            # update n_vars
            n_vars += 1
            varNames.append(var_name)

        assert(n_vars == len(sasp_vars))
        sasp_file.write("\n%s" % str(n_vars))
        sasp_file.write(''.join(sasp_vars))
        
    def write_mutex(self,sasp_file,varNames,varName2varNum, varName2Value):
        sasp_file.write("\n0") # no mutex groups
        
    def write_state(self,sasp_file,varNames,varName2varNum, varName2Value):
        
        # init state to zero
        init_vector = [0]*len(varNames)
        
        kLimit = self.opts.k_coBuchi()
        for k in range(kLimit):
            var_name = "(nxt %s %s)" % (k+1,k)
            var_num = varName2varNum[var_name]
            var_value = varName2Value[var_name]
            # update state value for variable
            init_vector[var_num] = var_value
        
        for aut_dynamics in self.automata:
            Q_Fin = aut_dynamics.aut.get_accepting()
            q_init = aut_dynamics.aut.get_init_state_number()

            for transition in aut_dynamics.aut.get_transitions():
                if transition.src_id == q_init:
                    var_name = "(poss %s)" % aut_dynamics.t(transition._id)
                    var_num = varName2varNum[var_name]
                    var_value = varName2Value[var_name]
                    # update state value for variable
                    init_vector[var_num] = var_value
        
            # var_name = "(oldCnt %s %s)" % (aut_dynamics.q(q_init), num(int(q_init in Q_Fin)))
            var_name = "(oldCnt %s %s)" % (aut_dynamics.q(q_init), num(0))
            var_num = varName2varNum[var_name]
            var_value = varName2Value[var_name]
            # update state value for variable
            init_vector[var_num] = var_value
        
        
        var_name = "(turn sync)"
        var_num = varName2varNum[var_name]
        var_value = varName2Value[var_name]
        # update state value for variable
        init_vector[var_num] = var_value

        
        # write initial state
        sasp_file.write("\nbegin_state\n%s\nend_state" % '\n'.join(str(var) for var in init_vector))
        
    
    def write_goal(self,sasp_file,varNames, varName2varNum, varName2Value):
        var_name = "(dummy_goal)"
        var_num = varName2varNum[var_name]
        var_value = varName2Value[var_name]

        sasp_file.write("\nbegin_goal\n1\n%s %s\nend_goal" % (var_num,var_value))
        
      
    def num_actions(self):
        n_actions = len(self.uncontrollable_vars) + 2*len(self.controllable_vars)
        
        kLimit = self.opts.k_coBuchi()
        for aut_dynamics in self.automata:
            transitions = aut_dynamics.aut.get_transitions()
            # n_actions += len(transitions)
            n_actions += len(transitions)*len(range(kLimit-1))
            # n_actions -= 1

            # Q_Fin = aut_dynamics.aut.get_accepting_dest()
            # count actions that lead to the goal because the counter reaches kLimit -> kLimit + 1
            # for t in transitions:
            #     if t.dst_id in Q_Fin:
            #         n_actions += 1
            Q_Fin = aut_dynamics.aut.get_accepting()
            transitions = aut_dynamics.aut.get_transitions() # state-based transitions in the automata
            # print(Q_Fin)
            # kLimit = self.opts.k_coBuchi()
            for t in transitions:
                if t.dst_id not in Q_Fin:
                    n_actions += 1                
        

        n_actions += 1 # start_sync_action
        
        # sync F(q)
        for aut_dynamics in self.automata:
            n_actions += aut_dynamics.aut.num_states() * len(range(kLimit))


        n_actions += 1 # continue_action

        return n_actions
        
    def write_actions(self,sasp_file,varNames, varName2varNum, varName2Value, mutexes):

        sasp_file.write("\n%s" % self.num_actions())
        
        self.write_environment_actions(sasp_file,varNames, varName2varNum, varName2Value, mutexes)
        self.write_controllable_vars_actions(sasp_file,varNames, varName2varNum, varName2Value, mutexes)
        self.write_automata_actions(sasp_file,varNames, varName2varNum, varName2Value, mutexes)
        self.write_start_sync_action(sasp_file,varNames, varName2varNum, varName2Value, mutexes)
        self.write_sync_F_actions(sasp_file,varNames, varName2varNum, varName2Value, mutexes)
        self.write_continue_action(sasp_file,varNames, varName2varNum, varName2Value, mutexes)
        return
    
    
    def write_axioms(self,sasp_file,varNames, varName2varNum, varName2Value):
        sasp_file.write("\n0") # no axioms
        
    def write_sasp_file(self):
        # if self.opts.sasp_output():
        from translator.action import SASpAction as Action
        self.Action = Action
        
        # elif self.opts.determinize_pddl():
        #     from translator.action import DetAction as Action
        #     self.Action = Action
        # else:
        #     from translator.action import FONDAction as Action
        #     self.Action = Action

        print("Writing SAS+ File")
        sasp_file = open(self.opts.output_domain(),"w")
        mutexes = compute_mutexes(self.controllable_vars + self.uncontrollable_vars, self.automata)
        
        self.write_version(sasp_file)
        self.write_metric(sasp_file)
        varNames = []
        varName2varNum = {}
        varName2Value = {}
        self.write_variables(sasp_file,varNames,varName2varNum, varName2Value) #todo
        self.write_mutex(sasp_file,varNames,varName2varNum, varName2Value) #checked
        self.write_state(sasp_file,varNames,varName2varNum, varName2Value) #done
        self.write_goal(sasp_file,varNames, varName2varNum, varName2Value) #checked
        self.write_actions(sasp_file,varNames, varName2varNum, varName2Value, mutexes) #done # when does mutexes apply? in write_automaton_actions?
        self.write_axioms(sasp_file,varNames, varName2varNum, varName2Value) #checked
        
        sasp_file.close()
        
        if self.opts.output_controller():
            controller_aux_file = open(self.opts.output_domain() + '.aux',"w")
            controller_aux_file.write("Controllable: %s\n" % ','.join(self.controllable_vars))
            controller_aux_file.write("Uncontrollable: %s\n" % ','.join(self.uncontrollable_vars))
            
            for aut_dynamics in self.automata:
                transitions = aut_dynamics.aut.get_transitions()
                for transition in transitions:
                    action_name = "move_aut_%s_t%s_%s_%s" % (aut_dynamics.aut_id, transition._id, transition.src_id, transition.dst_id)
                    controller_aux_file.write("Guard %s: %s\n" % (action_name,','.join(transition.guard)))
                    
            controller_aux_file.close()

    

    def write_automata_actions(self,sasp_file,varNames, varName2varNum, varName2Value, mutexes):
        for aut_dynamics in self.automata:
            self.write_automaton_actions(aut_dynamics,sasp_file,varNames, varName2varNum, varName2Value, mutexes)
        
        
    def write_automaton_actions(self,aut_dynamics,sasp_file,varNames, varName2varNum, varName2Value, mutexes):
        # Write actions to set Y variables via automaton transitions
        Q_Fin = aut_dynamics.aut.get_accepting()
        # Q_Fin = aut_dynamics.aut.get_accepting_dest() # trans-based transitions in the automata
        transitions = aut_dynamics.aut.get_transitions() # state-based transitions in the automata
        # print(Q_Fin)
        kLimit = self.opts.k_coBuchi()
        for t in transitions:
            # print("%s-->%s \t %s" % (t.src_id, t.dst_id,t.guard))
            for k in range(kLimit-1):
                action = self.write_transition_action(aut_dynamics, t, t.dst_id in Q_Fin, varNames, varName2varNum, varName2Value, mutexes,k)
                sasp_file.write(action.to_sasp())
            if t.dst_id not in Q_Fin:
                # write transitions that do not lead to accepting states and the counter stays in kLimit
                action = self.write_transition_action(aut_dynamics, t, t.dst_id in Q_Fin, varNames, varName2varNum, varName2Value, mutexes,kLimit)
                sasp_file.write(action.to_sasp())

    def write_transition_action(self, aut_dynamics, transition, goesto_accepting, varNames, varName2varNum, varName2Value, mutexes, k):
        
        action = self.Action()
        action.name = "move_aut_%s_t%s_%s_%s_%s" % (aut_dynamics.aut_id, transition._id, transition.src_id, transition.dst_id,k)
        outcome = []
        action.prevails = []

        # prevail turn variable
        var_name = "(turn y)"
        var_num = varName2varNum[var_name]
        var_value = varName2Value[var_name]
        prevail = (var_num,var_value)
        action.prevails.append( prevail )
        
        # update transition variables
        var_name = '(poss %s)' % aut_dynamics.t(transition._id)
        var_num = varName2varNum[var_name]
        var_value = varName2Value[var_name]
        effect = (var_num,var_value,0)
        outcome.append(effect)
        prevail = (var_num,var_value)
        action.prevails.append( prevail )

        var_name = '(oldCnt %s %s)' % (aut_dynamics.q(transition.src_id),num(k))
        var_num = varName2varNum[var_name]
        var_value = varName2Value[var_name]
        prevail = (var_num,var_value)
        action.prevails.append( prevail )

        # update automaton variables
        var_name = '(F %s)' % aut_dynamics.q(transition.dst_id)
        var_num = varName2varNum[var_name]
        var_value = varName2Value[var_name]
        effect = (var_num, -1, var_value)
        outcome.append(effect)

        # update trigger/goal variables
        
        if goesto_accepting:
            # update newCnt variables
            kLimit = self.opts.k_coBuchi()  
            
            assert( k+1 <= kLimit)
            # increment counter by one
            var_name = '(newCnt %s %s)' % (aut_dynamics.q(transition.dst_id),num(k+1))
            var_num = varName2varNum[var_name]
            var_value = varName2Value[var_name]
            effect = (var_num, -1, var_value)
            outcome.append(effect)
            # prevail = (var_num,var_value)
            # action.prevails.append( prevail )

            # else:
            #     # goal
            #     assert(k+1 >= kLimit)
            #     var_name = '(dummy_goal)'
            #     var_num = varName2varNum[var_name]
            #     var_value = varName2Value[var_name]
            #     effect = (var_num, -1, var_value)
            #     outcome.append(effect)
                
        else:
            assert(not goesto_accepting)
            # do not increment counter
            var_name = '(newCnt %s %s)' % (aut_dynamics.q(transition.dst_id),num(k))
            var_num = varName2varNum[var_name]
            var_value = varName2Value[var_name]
            effect = (var_num, -1, var_value)
            outcome.append(effect)

        # if self.opts.fixed_var_ordering():
        #     action.pre += ['(not (poss %s))' % aut_dynamics.t(j) for j in range(transition._id)]

        transition_mutexes = []
        for t in aut_dynamics.aut.get_transitions():
            if t.src_id == transition.src_id and t.dst_id == transition.dst_id and t._id != transition._id:
                transition_mutexes.append( aut_dynamics.t(t._id) )

        for literal in transition.guard:
            transition_mutexes += mutexes[literal]
            
        
        # update transition variables that are mutex
        for mutex in set(transition_mutexes):
            var_name = '(poss %s)' % mutex
            var_num = varName2varNum[var_name]
            var_value = varName2Value[var_name]
            effect = (var_num, -1, 0)
            outcome.append(effect)
        
        action.outcomes = [outcome]

        return action
        
    def write_start_sync_action(self, sasp_file,varNames, varName2varNum, varName2Value, mutexes):
        # Write action to switch mode from environment_mode to automaton_mode
        action = self.Action()
        action.name = "start_sync"
        outcome = []
        action.prevails = []
        var_name_from = "(turn y)"
        var_num_from = varName2varNum[var_name_from]
        var_value_from = varName2Value[var_name_from]
        var_name_to = "(turn sync)"
        var_num_to = varName2varNum[var_name_to]
        var_value_to = varName2Value[var_name_to]
        assert(var_num_from == var_num_to)
        effect = (var_num_from,var_value_from,var_value_to)
        outcome.append(effect)
        prevail = (var_num_from,var_value_from)
        action.prevails.append(prevail)
        
        for aut_dynamics in self.automata:
            for transition in aut_dynamics.aut.get_transitions():
                var_name = "(poss %s)" % aut_dynamics.t(transition._id)
                var_num = varName2varNum[var_name]
                var_value = varName2Value[var_name]
                prevail = (var_num,1-var_value)
                action.prevails.append(prevail)

        
        # counters
        kLimit = self.opts.k_coBuchi()
        for aut_dynamics in self.automata:
            for s in range(0, aut_dynamics.aut.num_states()):
                for k in range(kLimit):
                    var_name = "(oldCnt %s %s)" % (aut_dynamics.q(s),num(k))
                    var_num = varName2varNum[var_name]
                    effect = (var_num,-1,0)
                    outcome.append(effect)
        
        
        action.outcomes = [outcome]
        
        sasp_file.write(action.to_sasp())    

    def write_sync_F_actions(self, sasp_file,varNames, varName2varNum, varName2Value, mutexes):
        kLimit = self.opts.k_coBuchi()
        for aut_dynamics in self.automata:
            for n_q in range(aut_dynamics.aut.num_states()):
                for k in range(kLimit):
                    action = self.Action()
                    action.name = "sync_F_aut_%s_q_%s_%s" % (aut_dynamics.aut_id, n_q, k)
                    outcome = []
                    
                    # prevails
                    action.prevails = []
                    var_name = "(turn sync)"
                    var_num = varName2varNum[var_name]
                    var_value = varName2Value[var_name]
                    prevail = (var_num, var_value)
                    action.prevails.append(prevail)
                    
                    if self.opts.fixed_var_ordering():
                        prevail_var_names = ['(F %s)' % aut_dynamics.q(j) for j in range(n_q)]
                        for var_name in prevail_var_names:
                            var_num = varName2varNum[var_name]
                            prevail = (var_num, 0)
                            action.prevails.append(prevail)
                    
                    # update automaton variables
                    var_name = "(F %s)" %  aut_dynamics.q(n_q)
                    var_num = varName2varNum[var_name]
                    effect = (var_num, 1, 0)
                    outcome.append( effect )
                    prevail = (var_num, 1)
                    action.prevails.append(prevail)
    
                    for transition in aut_dynamics.aut.get_transitions():
                        if transition.src_id == n_q:
                            var_name = "(poss %s)" % aut_dynamics.t(transition._id)
                            var_num = varName2varNum[var_name]
                            var_value = varName2Value[var_name]
                            effect = (var_num, -1, var_value)
                            outcome.append( effect )
    
    
                    # oldCnt counters
                    var_name = "(oldCnt %s %s)" % (aut_dynamics.q(n_q),num(k))
                    var_num = varName2varNum[var_name]
                    var_value = varName2Value[var_name]
                    effect = (var_num, -1, var_value)
                    outcome.append( effect )
                    
                    # newCnt counters
                    var_name = "(newCnt %s %s)" % (aut_dynamics.q(n_q),num(k))
                    var_num = varName2varNum[var_name]
                    var_value = varName2Value[var_name]
                    prevail = (var_num, var_value)
                    action.prevails.append(prevail)
                    
                    for k2 in range(k+1,kLimit):
                        var_name = "(newCnt %s %s)" % (aut_dynamics.q(n_q),num(k2))
                        var_num = varName2varNum[var_name]
                        var_value = varName2Value[var_name]
                        prevail = (var_num, 1-var_value)
                        action.prevails.append(prevail)
                    for k2 in range(k+1):
                        var_name = "(newCnt %s %s)" % (aut_dynamics.q(n_q),num(k2))
                        var_num = varName2varNum[var_name]
                        var_value = varName2Value[var_name]
                        effect = (var_num, -1, 1-var_value)
                        outcome.append( effect )
                        
                    # effect = (var_num,-1,0)
                    # outcome.append(effect)
                    
                    action.outcomes = [outcome]
    
                    sasp_file.write(action.to_sasp())
                    

    def write_continue_action(self, sasp_file,varNames, varName2varNum, varName2Value, mutexes):
        action = self.Action()
        action.name = "continue"
        action.prevails = []
        outcome = []
        # update turn variable
        var_name_from = "(turn sync)"
        var_num_from = varName2varNum[var_name_from]
        var_value_from = varName2Value[var_name_from]
        if self.opts.game_semantics() == "mealy":
            var_name_to = "(turn %s)" % self.uncontrollable_vars[0]
        else:
            assert(self.opts.game_semantics() == "moore")
            var_name_to = "(turn %s)" % self.controllable_vars[0]
        var_num_to = varName2varNum[var_name_to]
        var_value_to = varName2Value[var_name_to]
        assert(var_num_from == var_num_to)
        effect = (var_num_from,var_value_from,var_value_to)
        outcome.append(effect)
        prevail = (var_num_from,var_value_from)
        action.prevails.append(prevail)
        
        # prevails for the automaton variables
        
        for aut_dynamics in self.automata:
            for j in range(aut_dynamics.aut.num_states()):
                var_name = "(F %s)" % aut_dynamics.q(j) 
                var_num = varName2varNum[var_name]
                prevail = (var_num,0)
                action.prevails.append(prevail)


        var_name = '(dummy_goal)'
        var_num = varName2varNum[var_name]
        var_value = varName2Value[var_name]
        effect = (var_num, -1, var_value)
        outcome2 = [effect]
        
        
        action.outcomes = [outcome,outcome2]
        sasp_file.write(action.to_sasp())
    

    def write_environment_actions(self, sasp_file,varNames, varName2varNum, varName2Value, mutexes):
        # Write cascade of actions to set X variables
        for i in range(len(self.uncontrollable_vars) ):
            if i < len(self.uncontrollable_vars) - 1:
                next_turn = self.uncontrollable_vars[i+1]
            else:
                if self.opts.game_semantics() == "mealy":
                    next_turn = self.controllable_vars[0]
                else:
                    assert(self.opts.game_semantics() == "moore")
                    next_turn = "y"
            
            effects1 = []
            effects2 = []
            
            action = self.Action()
            action.name = "env_move_%s" % self.uncontrollable_vars[i]
            action.prevails = []

            var_name_from = "(turn %s)" % self.uncontrollable_vars[i]
            var_name_to = "(turn %s)" % next_turn
            
            var_num_from = varName2varNum[var_name_from]
            var_num_to = varName2varNum[var_name_to]
            assert(var_num_from == var_num_to)
            
            var_value_from = varName2Value[var_name_from]
            var_value_to = varName2Value[var_name_to]
            
            # switching turns
            effect = (var_num_from, var_value_from, var_value_to)
            effects1.append( effect )
            effects2.append( effect )
            prevail = (var_num_from,var_value_from)
            action.prevails.append(prevail)
            
            # deeming unfeasible mutex transitions in effects1
            for transition in mutexes[self.uncontrollable_vars[i]]:
                var_name = "(poss %s)" % transition
                var_num = varName2varNum[var_name]
                effect = (var_num, -1, 0)
                effects1.append(effect)
                
                if self.opts.output_controller():
                    var_name = "(pos)"
                    var_num = varName2varNum[var_name]
                    effect = (var_num, -1, 1)
                    effects1.append(effect)

           # deeming unfeasible mutex transitions in effects2
            for transition in mutexes['!' + self.uncontrollable_vars[i]]:
                var_name = "(poss %s)" % transition
                var_num = varName2varNum[var_name]
                effect = (var_num, -1, 0)
                effects2.append(effect)
                
                if self.opts.output_controller():
                    var_name = "(pos)"
                    var_num = varName2varNum[var_name]
                    effect = (var_num, -1, 0)
                    effects2.append(effect)

            action.outcomes = [effects1,effects2]
            
            sasp_file.write(action.to_sasp())

    def write_controllable_vars_actions(self, sasp_file,varNames, varName2varNum, varName2Value, mutexes):

        for i in range(len(self.controllable_vars) ):
            if i < len(self.controllable_vars) - 1:
                next_turn = self.controllable_vars[i+1]
            else:
                if self.opts.game_semantics() == "mealy":
                    next_turn = "y"
                else:
                    assert(self.opts.game_semantics() == "moore")
                    next_turn = self.uncontrollable_vars[0]
            
            effects1 = []
            effects2 = []
            
            action1 = self.Action()
            action2 = self.Action()
            action1.name = "enable_var_%s" % self.controllable_vars[i]
            action2.name = "disable_var_%s" % self.controllable_vars[i]
            action1.prevails = []
            action2.prevails = []

            var_name_from = "(turn %s)" % self.controllable_vars[i]
            var_name_to = "(turn %s)" % next_turn
            
            var_num_from = varName2varNum[var_name_from]
            var_num_to = varName2varNum[var_name_to]
            assert(var_num_from == var_num_to)
            
            var_value_from = varName2Value[var_name_from]
            var_value_to = varName2Value[var_name_to]
            
            # switching turns
            effect = (var_num_from, var_value_from, var_value_to)
            effects1.append( effect )
            effects2.append( effect )
            prevail = (var_num_from,var_value_from)
            action1.prevails.append(prevail)
            action2.prevails.append(prevail)
            
           # deeming unfeasible mutex transitions in effects1
            for transition in mutexes[self.controllable_vars[i]]:
                var_name = "(poss %s)" % transition
                var_num = varName2varNum[var_name]
                effect = (var_num, -1, 0)
                effects1.append(effect)

           # deeming unfeasible mutex transitions in effects2
            for transition in mutexes['!' + self.controllable_vars[i]]:
                var_name = "(poss %s)" % transition
                var_num = varName2varNum[var_name]
                effect = (var_num, -1, 0)
                effects2.append(effect)

            action1.outcomes = [effects1]
            action2.outcomes = [effects2]
            
            sasp_file.write(action1.to_sasp())
            sasp_file.write(action2.to_sasp())
        
    # for statistical analysis
    def statistics(self):
        # number of ground fluents
        #
        n_fluents = 0
        n_fluents += sum(aut_dynamics.aut.num_states() for aut_dynamics in self.automata)
        n_fluents += sum( len(aut_dynamics.aut.get_transitions()) for aut_dynamics in self.automata)
        n_fluents += len(self.uncontrollable_vars) #+ len(self.controllable_vars)
        n_fluents += len(['y','sync'])
        n_fluents += len(['dummy_goal'])
        
        
        # number of ground actions
        
        n_actions = 0
        n_actions += len(self.uncontrollable_vars)
        n_actions += sum( len(aut_dynamics.aut.get_transitions()) for aut_dynamics in self.automata )
        n_actions += 1
            
        for aut_dynamics in self.automata:
            for n_q in range(aut_dynamics.aut.num_states()):
                n_actions += 1
                
        n_actions += 1
    
        return n_fluents,n_actions
            
    def num_actions_in_subtree(self,node):
        n = 0
        if node.connective == "and":
            n = 1
        elif node.connective == "or":
            n = len(node.siblings)
            
        if node.siblings != None:
            for sibling in node.siblings:
                n += self.num_actions_in_subtree(sibling)
        
        return n
                
        