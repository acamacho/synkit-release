class FONDAction:
    name = None
    param = None
    pre = None
    effs = None
    
    def to_pddl(self):
        assert(len(self.effs) > 0)
        pddl = "\n\t(:action %s" % self.name
        if self.param:
            pddl += "\n\t\t:parameters %s" % self.param
        precs_pddl = "(and %s)" % (" ".join(self.pre))
        if len(self.effs) > 1:
            effs_pddl = "(oneof \n\t\t\t" + "\n\t\t\t".join( "(and %s)" %  " ".join(eff_k) for eff_k in self.effs  ) + "\n\t\t\t)\n\t\t"
        elif len(self.effs) == 1:
            effs_pddl = "(and %s)" % " ".join(self.effs[0])
        pddl += "\n\t\t:precondition %s" % precs_pddl
        pddl += "\n\t\t:effect %s" % effs_pddl
        pddl +="\n\t)"

        return pddl

class DetAction:
    name = None
    param = None
    pre = None
    effs = None
    
    def to_pddl(self):
        assert(len(self.effs) > 0)
        pddl = ""
        k = 1
        for eff_k in self.effs:
            if len(self.effs) > 1:
                pddl += "\n\t(:action D_%s_%s" % (k,self.name)
            else:
                assert(len(self.effs) == 1)
                pddl += "\n\t(:action %s" % self.name
            if self.param:
                pddl += "\n\t\t:parameters %s" % self.param
            precs_pddl = "(and %s)" % (" ".join(self.pre))
            effs_pddl = "(and %s)" % " ".join(self.effs[k-1])
            pddl += "\n\t\t:precondition %s" % precs_pddl
            pddl += "\n\t\t:effect %s" % effs_pddl
            pddl +="\n\t)"
            k += 1

        return pddl

class SASpAction:
    name = None
    prevails = None
    outcomes = None
    
    def to_sasp(self):
        sasp = "\nbegin_operator"
        sasp += "\n%s" % self.name
        
        # compute prevails, the preconditions that are not in the effects (what happens if a prevail is only in one effect?)
        # perhaps start by ommitting the prevails section and include everything within the effects
        if self.prevails != None:
            assert(len(self.prevails) > 0)
            sasp += "\n%s" % len(self.prevails) # number of prevails
            for variable,value in self.prevails:
                sasp += "\n%s %s" % (variable,value) # write prevails
                continue
        else:
            sasp += "\n0" # zero prevails
            
        # number of outcomes
        assert(len(self.outcomes) > 0)
        sasp += "\n%s" % len(self.outcomes) # number of outcomes
        for outcome in self.outcomes:
            sasp += "\n%s" % len(outcome) # number of variables affected in the outcome
            for variable,fromvalue,tovalue in outcome:
                sasp += "\n0 %s %s %s" % (variable,fromvalue,tovalue)
        

        sasp += "\n0" # operator cost

        sasp += "\nend_operator"

        return sasp