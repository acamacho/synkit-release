#!/usr/bin/env python3

import pddl
from pddl import Action
from automata import *
import spot
spot.setup()


    
def write_pddl_domain(X, Y, aut, max_horizon, output):
    
    pddl_file = open(output,"w")
    
    
    # fluents controlling the search horizon
    types = "\n\t" + " counter"
    
    predicates = " \n\t" + "(at-horizon ?h - counter) (next ?h1 - counter ?h2 - counter) (first-horizon ?h - counter) "
    
    # variable fluents: "literals"
    predicates += " \n\t" + " ".join("(pos_%s)" % x for x in X)
    predicates += " \n\t" + " ".join("(neg_%s)" % x for x in X)
    predicates += " \n\t" + " ".join("(pos_%s)" % y for y in Y)
    predicates += " \n\t" + " ".join("(neg_%s)" % y for y in Y)
    
    # automaton fluents, sync, tokens, sync-tokens
    predicates += " \n\t" + " ".join("(q_%s)" % k for k in range(0, aut.num_states()))
    predicates += " \n\t" + " ".join("(qs_%s)" % k for k in range(0, aut.num_states()))
    predicates += " \n\t" + " ".join("(qt_%s)" % k for k in range(0, aut.num_states()))
    predicates += " \n\t" + " ".join("(qst_%s)" % k for k in range(0, aut.num_states()))
    
    # fluents to control modes
    predicates += "\n\t" + "(automaton_mode) (environment_mode)"
    
    # fluents to control turns for uncontrollable variable assignments
    predicates += " \n\t" + " ".join("(varturn_%s)" % x for x in X)
    
    
    # fluent to control accepting states
    predicates += "\n\t" + "(can_accept)"
    
    # dummy goal fluent
    predicates += "\n\t" + "(dummy_goal)"
    
    # Write header
    header = """(define (domain dom)
    (:requirements :typing :strips :negative-preconditions :non-deterministic)
    (:types %s)
    (:predicates %s)
    """ % (types,predicates)
    
    pddl_file.write(header)
    
    # Write cascade of actions to set X variables
    for i in range(len(X) -1):
        action = Action()
        action.name = "env_move_%s" % X[i]
        action.pre = ["(environment_mode)", "(varturn_%s)" % X[i] ]
        eff1 = ["(pos_%s)" % X[i], "(not (varturn_%s))" % X[i], "(varturn_%s)" % X[i+1] ]
        eff2 = ["(neg_%s)" % X[i], "(not (varturn_%s))" % X[i], "(varturn_%s)" % X[i+1] ]
        action.effs = [eff1,eff2]
        pddl_file.write(action.to_pddl())
    
    action = Action()
    action.name = "env_move_%s" % X[len(X) -1]
    action.pre = ["(environment_mode)", "(varturn_%s)" % X[len(X) -1] ]
    eff1 = ["(pos_%s)" % X[len(X) -1], "(not (varturn_%s))" % X[len(X) -1] ]
    eff2 = ["(neg_%s)" % X[len(X) -1], "(not (varturn_%s))" % X[len(X) -1] ]
    action.effs = [eff1,eff2]
    pddl_file.write(action.to_pddl())
    
    # Write action to switch mode from environment_mode to automaton_mode
    action = Action()
    action.name = "switch_environment_to_automaton_mode"
    action.param = "(?h1 ?h2 - counter)"
    action.pre = ["(environment_mode)"]
    action.pre += ["(at-horizon ?h1)", "(next ?h2 ?h1)"]
    action.pre += ["(not (varturn_%s))" % x for x in X]
    eff = ["(not (environment_mode))", "(automaton_mode)"] 
    eff += ["(when (q_%s) (and (qs_%s) (not (q_%s)) )  )" % (state, state, state) for state in range(0, aut.num_states()) ]
    eff += ["(when (qt_%s) (and (qst_%s) (not (qt_%s)) )  )" % (state, state, state) for state in range(0, aut.num_states()) ]
    eff += ["(not (at-horizon ?h1))", "(at-horizon ?h2)"]
    action.effs = [eff]
    pddl_file.write(action.to_pddl())       
    
    
    
    # Write actions to set Y variables via automaton transitions
    # Q_Fin = get_accepting(aut)
    Q_Fin = aut.get_accepting()
    
    # print(Q_Fin)
    # raise error
    # print(aut.to_str('hoa'))

    # transitions = get_transitions(aut)
    transitions = aut.get_transitions()
    for t in transitions:
        action = pddl.get_transition_action(t, t.dst_id in Q_Fin )
        pddl_file.write(action.to_pddl())


    # Write action to switch mode, deliveratively, from  automaton_mode to environment_mode
    action = Action()
    action.name = "switch_automaton_to_environment_mode"
    # action.param = "(?h1 ?h2 - counter)"
    action.pre = ["(automaton_mode)"]
    # action.pre = ["(automaton_mode)", "(at-horizon ?h1)", "(next ?h2 ?h1)"]
    # action.pre += ["(not (at-horizon ?h%s))" % (max_horizon -1)]
    eff = ["(not (automaton_mode))", "(environment_mode)"] 
    eff += ["(varturn_%s)" % X[0] ]
    # Regularization
    eff += ["(not (qs_%s))" % state for state in range(0, aut.num_states()) ]
    eff += ["(not (qst_%s))" % state for state in range(0, aut.num_states()) ]
    eff += ["(not (pos_%s))" % x for x in X ]
    eff += ["(not (neg_%s))" % x for x in X ]
    eff += ["(not (pos_%s))" % y for y in Y ]
    eff += ["(not (neg_%s))" % y for y in Y ]
    # eff += ["(not (at-horizon ?h1))", "(at-horizon ?h2)"]
    # eff += ["(when (at-horizon ?h%s) (at-horizon ?h%s) )" % (h,h+1) for h in range(max_horizon-1)]
    action.effs = [eff]
    pddl_file.write(action.to_pddl())       

     # Write action to advance horizon counter, deliveratively
    action = Action()
    action.name = "advance_horizon"
    action.param = "(?h1 ?h2 - counter)"
    action.pre = ["(automaton_mode)", "(at-horizon ?h1)", "(next ?h2 ?h1)"]
    eff = ["(not (at-horizon ?h1))", "(at-horizon ?h2)"]
    action.effs = [eff]
    pddl_file.write(action.to_pddl())       

    # Write action to recognize accepting states
    # todo: make sure that we copy q -> qT for non-accepting states.
    action = Action()
    action.name = "accept"
    action.param = "(?h ?h0 - counter)"
    action.pre = ["(automaton_mode)","(can_accept)"]
    action.pre += ["(at-horizon ?h)", "(first-horizon ?h0)"]
    eff1 = ["(dummy_goal)"]
    eff2 = ["(not (can_accept))"]
    eff2 += ["(varturn_%s)" % X[0] ]
    eff2 += ["(not (automaton_mode))", "(environment_mode)"]
    # Regularization
    eff2 += ["(not (qs_%s))" % state for state in range(0, aut.num_states()) ]
    eff2 += ["(not (qst_%s))" % state for state in range(0, aut.num_states()) ]
    eff2 += ["(not (pos_%s))" % x for x in X ]
    eff2 += ["(not (neg_%s))" % x for x in X ]
    eff2 += ["(not (pos_%s))" % y for y in Y ]
    eff2 += ["(not (neg_%s))" % y for y in Y ]
    # eff2 += ["(when (q_%s) (and (qt_%s) (not (q_%s)) )  )" % (state, state, state) for state in range(0, aut.num_states()) ]
    # eff2 += ["(when (qt_%s) (and (not (qt_%s)) )  )" % (state, state) for state in range(0, aut.num_states()) ]
    # eff2 += ["(when (and (q_%s) (not (qt_%s))) (qt_%s) )" % (state, state, state) for state in range(0, aut.num_states()) ]
    eff2 += ["(when (q_%s) (qt_%s) )" % (state, state) for state in range(0, aut.num_states()) ]
    eff2 += ["(when (qt_%s) (and (not (qt_%s)) (not (q_%s)) )  )" % (state, state, state) for state in range(0, aut.num_states()) ]
    eff2 += ["(not (at-horizon ?h))", "(at-horizon ?h0)"]
    # eff2 += ["(not (at-horizon ?h%s) )" % (h) for h in range(1, max_horizon)]
    action.effs = [eff1,eff2]
    pddl_file.write(action.to_pddl())

    pddl_file.write("\n)")
    pddl_file.close()



def write_pddl_instance(X,Y, aut, max_horizon, output):
    
    pddl_file = open(output,"w")
    
    # fluents controlling the search horizon
    objects = "\n\t" + " ".join("h%s" % h for h in range(0, max_horizon) ) + " - counter"
    
    # horizon 0
    init = "(at-horizon h0) (first-horizon h0)"
    # 
    # q_init = aut.get_init_state_number()
    q_init = aut.get_init_state_number()
    init += "\n\t" + " ".join("(next h%s h%s)" % (h+1,h) for h in range(0, max_horizon -1) )
    init += "\n\t" + "(environment_mode)"
    init += "\n\t" + "(varturn_%s)" % X[0]
    init += "\n\t" + "(q_%s)" % q_init

    
    #todo: at initial state, at-horizon ?h0
    header = """(define (problem prob)
    (:domain dom)
  (:objects %s)
  (:init %s)
  (:goal (dummy_goal))
 )""" %(objects, init)

    pddl_file.write(header)
    pddl_file.close()


if __name__ == "__main__":
    import optparse
    parser = optparse.OptionParser()
    
    parser.add_option('--spec',
        action="store", dest="spec",
        help="LTL specification in format ltlxba string", default="")
    parser.add_option('--ins',
        action="store", dest="ins",
        help="Input (uncontrollable) variables in the LTL specification", default="")
    parser.add_option('--outs',
        action="store", dest="outs",
        help="Output (controllable) variables in the LTL specification", default="")
    parser.add_option('--output',
        action="store", dest="output",
        help="Output name of the PDDL domain and instance", default="")
    parser.add_option('--horizon',
        action="store", dest="horizon", type=int,
        help="Search horizon parameter", default=5)
        
    options, args = parser.parse_args()
    
    X = [x.replace(" ","") for x in options.ins.split(",")]
    Y = [y.replace(" ","") for y in options.outs.split(",")]
    spec = options.spec
    
    aut = Automaton(spec)
    Q_Fin = aut.get_accepting()
    # aut = ltl2aut(spec)
    # Q_Fin = get_accepting(aut)
    # transitions = get_transitions(aut)
    
    output_domain = "%s_domain.pddl" % options.output
    output_instance = "%s_instance.pddl" % options.output
    
    max_horizon = options.horizon
    write_pddl_domain(X,Y,aut,max_horizon,output_domain)
    write_pddl_instance(X,Y,aut,max_horizon,output_instance)


def test():
    X = ['x1', 'x2']
    Y = ['y1','y2']
    spec = "GF x1 -> GF(x2 || (y1 && y2))"
    
    aut = ltl2aut(spec)
    Q_Fin = get_accepting(aut)
    transitions = get_transitions(aut)
    
    
    for t in transitions:
        print(t.guard)
        
    for t in transitions:
        action = get_transition_action(t, t.dst_id in Q_Fin )
        # print(action.name)
        # print(action.pre)
        # print(action.eff)
        print(action.to_pddl())
        # raise error
        
    output_domain = "test_domain.pddl"
    output_instance = "test_instance.pddl"
    max_horizon = 10
    write_pddl_domain(X,Y,aut,max_horizon,output_domain)
    write_pddl_instance(X,Y,aut,max_horizon,output_instance)
