# This file is part of SynKit, a tool for LTL synthesis via automated planning.
# Copyright (C) 2018  Alberto Camacho <acamacho@cs.toronto.edu>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import subprocess
# from krrt.utils import get_value

PRP_PARAMS = {'best': { '--jic-limit': [18000],
                        '--trials': [100],
                        '--forgetpolicy': [0],
                        '--fullstate': [0],
                        '--planlocal': [1],
                        '--partial-planlocal': [1],
                        '--plan-with-policy': [1],
                        '--limit-planlocal': [1],
                        '--detect-deadends': [1],
                        '--generalize-deadends': [1],
                        '--online-deadends': [1],
                        '--optimized-scd': [1]},

              'no-local': { '--jic-limit': [18000],
                        '--trials': [100],
                        '--forgetpolicy': [0],
                        '--fullstate': [0],
                        '--planlocal': [1],
                        '--partial-planlocal': [0],
                        '--plan-with-policy': [1],
                        '--limit-planlocal': [0],
                        '--detect-deadends': [1],
                        '--generalize-deadends': [1],
                        '--online-deadends': [1],
                        '--optimized-scd': [1]},

              'no-scd': { '--jic-limit': [18000],
                        '--trials': [100],
                        '--forgetpolicy': [0],
                        '--fullstate': [0],
                        '--planlocal': [1],
                        '--partial-planlocal': [1],
                        '--plan-with-policy': [1],
                        '--limit-planlocal': [1],
                        '--detect-deadends': [1],
                        '--generalize-deadends': [1],
                        '--online-deadends': [1],
                        '--optimized-scd': [0]},

              'no-dead': { '--jic-limit': [18000],
                        '--trials': [100],
                        '--forgetpolicy': [0],
                        '--fullstate': [0],
                        '--planlocal': [1],
                        '--partial-planlocal': [1],
                        '--plan-with-policy': [1],
                        '--limit-planlocal': [1],
                        '--detect-deadends': [1],
                        '--generalize-deadends': [0],
                        '--online-deadends': [0],
                        '--optimized-scd': [1]},

              'no-partial': { '--jic-limit': [18000],
                           '--trials': [100],
                           '--forgetpolicy': [0],
                           '--fullstate': [1],
                           '--planlocal': [1],
                           '--partial-planlocal': [0],
                           '--plan-with-policy': [1],
                           '--limit-planlocal': [1],
                           '--detect-deadends': [1],
                           '--generalize-deadends': [0],
                           '--online-deadends': [1],
                           '--optimized-scd': [1]},

              'jic':  { '--jic-limit': [],
                        '--trials': [0],
                        '--forgetpolicy': [0],
                        '--fullstate': [0],
                        '--planlocal': [1],
                        '--partial-planlocal': [1],
                        '--plan-with-policy': [1],
                        '--limit-planlocal': [1],
                        '--detect-deadends': [1],
                        '--generalize-deadends': [1],
                        '--online-deadends': [1],
                        '--optimized-scd': [1]},

              'full': { '--jic-limit': [18000],
                        '--trials': [100],
                        '--forgetpolicy': [0],
                        '--fullstate': [0],
                        '--planlocal': [0,1],
                        '--partial-planlocal': [0,1],
                        '--plan-with-policy': [1],
                        '--limit-planlocal': [0,1],
                        '--detect-deadends': [0,1],
                        '--generalize-deadends': [0,1],
                        '--online-deadends': [0,1],
                        '--optimized-scd': [0,1]},

              'best-online': { '--jic-limit': [0],
                               '--trials': [100],
                               '--forgetpolicy': [1],
                               '--fullstate': [0],
                               '--planlocal': [1],
                               '--partial-planlocal': [1],
                               '--plan-with-policy': [1],
                               '--limit-planlocal': [1],
                               '--detect-deadends': [0],
                               '--generalize-deadends': [0],
                               '--online-deadends': [0],
                               '--optimized-scd': [0]},

              'ffreplan': { '--jic-limit': [0],
                            '--trials': [100],
                            '--forgetpolicy': [1],
                            '--fullstate': [1],
                            '--planlocal': [0],
                            '--partial-planlocal': [0],
                            '--plan-with-policy': [1],
                            '--limit-planlocal': [0],
                            '--detect-deadends': [0],
                            '--generalize-deadends': [0],
                            '--online-deadends': [0],
                            '--optimized-scd': [0]},

              'redundant': { '--jic-limit': [18000],
                             '--trials': [100],
                             '--forgetpolicy': [0],
                             '--fullstate': [0,1],
                             '--planlocal': [0,1],
                             '--partial-planlocal': [0],
                             '--plan-with-policy': [1],
                             '--limit-planlocal': [0],
                             '--detect-deadends': [0],
                             '--generalize-deadends': [0],
                             '--online-deadends': [0],
                             '--optimized-scd': [0]},

              'planlocal': { '--jic-limit': [18000],
                             '--trials': [100],
                             '--forgetpolicy': [0],
                             '--fullstate': [0],
                             '--planlocal': [0,1],
                             '--partial-planlocal': [0,1],
                             '--plan-with-policy': [1],
                             '--limit-planlocal': [0,1],
                             '--detect-deadends': [1],
                             '--generalize-deadends': [1],
                             '--online-deadends': [1],
                             '--optimized-scd': [1]},

              'test': { '--jic-limit': [0,18000],
                        '--trials': [10],
                        '--forgetpolicy': [0,1],
                        '--fullstate': [0,1],
                        '--planlocal': [0,1],
                        '--partial-planlocal': [0,1],
                        '--plan-with-policy': [0,1],
                        '--limit-planlocal': [0,1],
                        '--detect-deadends': [0,1],
                        '--generalize-deadends': [0,1],
                        '--online-deadends': [0,1],
                        '--optimized-scd': [0,1]}
             }
             
class Stats:
    solution_found = None
    runtime = None
    policy_time = None
    policy_size = None
    n_variables = None
    n_autstate_variables = None
    
# modified from krrt.utils
# bitbucket.org/haz/krtoolkit
def get_value(text, regex, value_type = float):
    """ Lifts a value from a file based on a regex passed in. """
    import re
    # Get the text of the time
    pattern = re.compile(regex, re.MULTILINE)
    results = pattern.search(text)
    if results:
        return value_type(results.group(1))
    else:
        print("Could not find the value, %s, in the text provided" % (regex))
        return(value_type(0.0))
        
# modified from krrt.utils
# bitbucket.org/haz/krtoolkit
def match_value(text, regex):
    """ Checks if the regex matches the contents of a specified file. """
    import re
    # Get the text of the time
    pattern = re.compile(regex, re.MULTILINE)
    if pattern.search(text):
        return True
    else:
        return False

# modified from prp's policy_experiment.py
def parse_prp(outfile,stats):
    no_solution = match_value(outfile, '.*No solution -- aborting repairs.*')
    if no_solution:
        stats.solution_found = False
        stats.runtime = 0.0
        stats.policy_time = 0.0
        stats.policy_size = 0
        return stats

    runtime = get_value(outfile, '.*Total time: ([0-9]+\.?[0-9]*)s\n.*', float)
    jic_time = get_value(outfile, '.*Just-in-case Repairs: ([0-9]+\.?[0-9]*)s\n.*', float)
    policy_use_time = get_value(outfile, '.*Using the policy: ([0-9]+\.?[0-9]*)s\n.*', float)
    policy_construction_time = get_value(outfile, '.*Policy Construction: ([0-9]+\.?[0-9]*)s\n.*', float)
    policy_eval_time = get_value(outfile, '.*Evaluating the policy quality: ([0-9]+\.?[0-9]*)s\n.*', float)
    search_time = get_value(outfile, '.*Search Time: ([0-9]+\.?[0-9]*)s\n.*', float)
    engine_init_time = get_value(outfile, '.*Engine Initialization: ([0-9]+\.?[0-9]*)s\n.*', float)
    regression_time = get_value(outfile, '.*Regression Computation: ([0-9]+\.?[0-9]*)s\n.*', float)
    simulator_time = get_value(outfile, '.*Simulator time: ([0-9]+\.?[0-9]*)s\n.*', float)

    successful_states = get_value(outfile, '.*Successful states: ([0-9]+\.?[0-9]*) \+.*', float)
    replans = get_value(outfile, '.*Replans: ([0-9]+\.?[0-9]*) \+.*', float)
    actions = get_value(outfile, '.*Actions: ([0-9]+\.?[0-9]*) \+.*', float)
    size = get_value(outfile, '.*State-Action Pairs: (\d+)\n.*', int)

    strongly_cyclic = match_value(outfile, '.*Strongly Cyclic: True.*')
    succeeded = get_value(outfile, '.*Succeeded: (\d+) .*', int)

    policy_score = get_value(outfile, '.*Policy Score: ([0-9]+\.?[0-9]*)\n.*', float)

    stats.solution_found = strongly_cyclic
    stats.runtime = runtime
    stats.policy_time = policy_construction_time
    stats.policy_size = size

def run(bin_path, domain_path, instance_path):
    prp_params = " ".join( "%s %s" % (key, PRP_PARAMS['best'][key][0]) for key in PRP_PARAMS['best'])
    planner_command = "%s %s %s %s" % (bin_path, domain_path, instance_path, prp_params)
    # print(planner_command)
    # raise error

    p = subprocess.Popen([planner_command], stdout=subprocess.PIPE, shell=True)
    planner_output, err = p.communicate()
    planner_output = str(planner_output, 'utf-8').strip()
    
    stats = Stats
    parse_prp(planner_output,stats)
    
    return stats
    