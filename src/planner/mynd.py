# This file is part of SynKit, a tool for LTL synthesis via automated planning.
# Copyright (C) 2018  Alberto Camacho <acamacho@cs.toronto.edu>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import subprocess
# from krrt.utils import get_value
import os
cwd = os.getcwd()
sasp_file = os.path.join(cwd,'output.sas')
 
class Stats:
    solution_found = None
    runtime = None
    policy_time = None
    policy_size = None
    nodes_visited = None
    n_variables = None
    n_autstate_variables = None
    
# modified from krrt.utils
# bitbucket.org/haz/krtoolkit
def get_value(text, regex, value_type = float):
    """ Lifts a value from a file based on a regex passed in. """
    import re
    # Get the text of the time
    pattern = re.compile(regex, re.MULTILINE)
    results = pattern.search(text)
    if results:
        return value_type(results.group(1))
    else:
        print("Could not find the value, %s, in the text provided" % (regex))
        return(value_type(0.0))
        
# modified from krrt.utils
# bitbucket.org/haz/krtoolkit
def match_value(text, regex):
    """ Checks if the regex matches the contents of a specified file. """
    import re
    # Get the text of the time
    pattern = re.compile(regex, re.MULTILINE)
    if pattern.search(text):
        return True
    else:
        return False

# modified from prp's policy_experiment.py
def parse_mynd(outfile,stats):

    strongly_cyclic = match_value(outfile, '.*Result: Strong plan found.*')
    runtime = get_value(outfile, '.*Time needed:[\s]*([0-9]+\.?[0-9]*)\sseconds.\n.*', float)
    search_time = get_value(outfile, '.*Time needed for search:[\s]*([0-9]+\.?[0-9]*)\sseconds.\n.*', float)
    if strongly_cyclic:
        size = get_value(outfile, '.*Policy entries:[\s]*(\d+)\n.*', int)
        # print(outfile)
        # stats.plan = plan
    else:
        size = -1
    nodes_visited = get_value(outfile, '.*Out of\s(\d+)\snodes.*', int)

    stats.solution_found = strongly_cyclic
    stats.runtime = search_time
    stats.policy_time = -1
    stats.policy_size = size
    stats.nodes_visited = nodes_visited

def parse_translator(outfile,stats):
    n_variables = get_value(outfile, '.*Translator variables:[\s]*(\d+)\n.*', int)
    stats.n_variables = n_variables
    
def parse_sasplus(sasp_file,stats):
    f = open(sasp_file)
    outfile = f.read()
    sasp_variables = outfile.split('end_variable')
    sasp_variables.pop() # delete last element, that does not have information about variables
    # print(sasp_variables)
    # raise error
    if len(sasp_variables) > 0:
        sasp_variables[0] = sasp_variables[0].split('begin_variable')[1]
    
    n_autstate_variables = 0
    for sasp_variable in sasp_variables:
        if "Atom f(aut" in sasp_variable:
            n_autstate_variables += 1
            # print(sasp_variable)
            # raise error
    stats.n_autstate_variables = n_autstate_variables
    # print(n_autstate_variables)
    # raise error
    f.close()

def run(bin_path, domain_path, instance_path):
    
    """
    translator_command = "python %s/../translator-fond/translate.py %s %s" % (bin_path, domain_path, instance_path)
    print("\tRunning Translation")
    print(translator_command)
    p = subprocess.Popen([translator_command], stdout=subprocess.PIPE, shell=True)
    translator_output, err = p.communicate()
    translator_output = str(translator_output, 'utf-8').strip()
    # print(translator_output);
    # raise err
    print("\tFinished translation. Running Planner")
    # planner_command = "ulimit -t 10; java -classpath %s -ea -Xmx1g mynd.MyNDPlanner -aostar -ff output.sas " % bin_path
    planner_command = "java -classpath %s -ea -Xmx1g mynd.MyNDPlanner -aostar -ff output.sas " % bin_path
    # planner_command = "java -classpath %s -ea -Xmx4g mynd.MyNDPlanner -aostar -pdbs output.sas " % bin_path
    # planner_command = "java -classpath %s -ea -Xmx4g mynd.MyNDPlanner -aostar -hmax output.sas " % bin_path
    # planner_command = "java -classpath %s -ea -Xmx4g mynd.MyNDPlanner -aostar -zero output.sas " % bin_path
    """
    sasp_file = domain_path
    dot_file = domain_path + ".plan.dot"
    planner_command = "java -classpath %s -ea -Xmx1g mynd.MyNDPlanner -aostar -ff -dumpPlan %s && mv %s %s" % (bin_path,sasp_file, "plan.dot.dot", dot_file)
    print(planner_command)
    # raise error

    p = subprocess.Popen([planner_command], stdout=subprocess.PIPE, shell=True)
    planner_output, err = p.communicate()
    planner_output = str(planner_output, 'utf-8').strip()
    # print(planner_output)
    stats = Stats
    # parse_translator(translator_output,stats)
    parse_sasplus(sasp_file,stats)
    parse_mynd(planner_output,stats)
    
    return stats
    