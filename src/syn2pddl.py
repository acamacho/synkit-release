# This file is part of SynKit, a tool for LTL synthesis via automated planning.
# Copyright (C) 2018  Alberto Camacho <acamacho@cs.toronto.edu>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python3

from parser.parser import Parser
from translator.automata import Automaton
from os import system

SAVE_STATITSTICS = True

def main(opts, automata_alanet = None):
    print("Parsing TLSF file %s" % opts.tlsf)
    parser = Parser(opts.tlsf)
    X = parser.uncontrollable_vars()
    Y = parser.controllable_vars()

    # importing the desired compilation
    if opts.game_type == "nfw":
        if opts.split_spec in ['tlsf_components']:
            from translator.nfw_conj_game_2_planning import PDDLDynamics
        else:
            from translator.nfw_game_2_planning import PDDLDynamics
    elif opts.game_type == "u0cw":
        if opts.split_spec in ['tlsf_components']:
            from translator.cufw_disj_game_2_planning import PDDLDynamics
        else:
            from translator.cufw_game_2_planning import PDDLDynamics
    elif opts.game_type == "ukcw":
        from translator.ukcw_game_2_planning import PDDLDynamics
    elif opts.game_type == "nkbw":
        from translator.nkbw_game_2_planning import PDDLDynamics

    elif opts.game_type == "nbw": # compilation presented at Genplan-17 and CCAI-18
        from translator.pddl_dynamics_automaton_explicit import PDDLDynamics
    else:
        raise("Automaton compilation not supported.")
    
    if not automata_alanet:
        automata_alanet = parser.get_alanet(opts)
    automata = automata_alanet.automata
    
    if opts.from_ltlf:
        assert(opts.aut_type == "nfw")
    
    if opts.semantics == opts.game_semantics:
        pddlDynamics = PDDLDynamics(X,Y,opts)
    else:
        assert( opts.semantics != opts.game_semantics)
        pddlDynamics = PDDLDynamics(Y,X,opts)

    for aut in automata:
        pddlDynamics.add_automaton(aut)
    
    pddlDynamics.write_pddl_domain()
    pddlDynamics.write_pddl_instance()

    if SAVE_STATITSTICS:
        n_fluents,n_actions = pddlDynamics.statistics()
        return n_fluents,n_actions

if __name__ == "__main__":
    import options
    
    opts = options.parse_options()
    main(opts)